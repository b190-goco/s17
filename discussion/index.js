console.log("Hello World!");


// FUNCTION DECLARATION
/*
	function - defines function in js
	printName() - function name, follows camel casing
	function block {} - inside are body functions, codes to be executed
*/
/*
	SYNTAX: function functionName(){
		code block;
	};
*/
function printName(){
	console.log("My name is Jygs.");
};
// declared only, not yet shown in CONSOLE unless called.

// CALL/INVOKE
/*
	run the codes of called/invoked function
*/

printName();

declaredFunction(); //hoisting
function declaredFunction(){
	console.log("Hello from declaredFunction!");
}

// FUNCTION EXPRESSION
// function stored in a variable
// no HOISTING
let variableFunction = function(){
	console.log("Hello again!");
}

variableFunction();

/*MINI ACTIVITY*/
/*
	create 2 function
		1) 3 anime reco
		2) 3 movie reco
*/

function animeRecommendation(){
	console.log("TOP 3 ANIME RECOMMENADTIONS:")
	console.log("Castlevania");
	console.log("Bofuri: I Don't Want to Get Hurt, so I'll Max Out My Defense");
	console.log("Asobi Asobase");	
}

function movieRecommendation(){
	console.log("TOP 3 MOVIE RECOMMENADTIONS:")
	console.log("Don't Look Up");
	console.log("Gone Girl");
	console.log("Ratched");	
}

movieRecommendation();
animeRecommendation();

// REASIGNING FUNCTION
// works, unless const
declaredFunction = function(){
	console.log("updated declaredFunction...");
};

declaredFunction();

const constfunction = function(){
	console.log("initialized const function...");
};

constfunction();

/*constfunction = function(){
	console.log("cannot be re-assigned");
};

constfunction();*/
// wont work since constfunction is declared CONST

// FUNCTION SCOPING
// accessibility of the function

let globalVar =  "Mr. Worldwide";
{
	let localVar = "Armando Perez";
	console.log(localVar);// error: localVa not defined, out of scope
	console.log(globalVar);
}

//console.log(localVar);// error: localVa not defined, out of scope

function showNames(){
	var functionVar = "Joe";
	const functionConst = "John";
	let functionLet = "Jane";
	console.log(functionVar);
	console.log(functionConst);
	console.log(functionLet);
	// all these variables only exist here!
};

showNames();
/*console.log(functionVar);
console.log(functionConst);
console.log(functionLet);*/

function newFunction(){
	let name = "Jane";

	function nestedFunction(){
		let nestedName = "John";
		console.log(name);// valid, name is declared outside
		console.log(nestedName);
	}
	nestedFunction();
	//console.log(nestedName); // error: declared in lower heirarchy
}
newFunction();

/*MINI ACTIVITY*/
/*
	create global-scoped variable
	create a function
		create let variable
		log
	call
*/

let everything = "A";
function everywhere(){
	let allatonce = "B";
	console.log(everything);
	console.log(allatonce); 
}
everywhere();

// USING ALERT
// alert () - small window at top, shows console message on browser

alert("Hello World!");

function showSampleAlert(){
	alert("Hello Again!");
};

showSampleAlert();

console.log("I will be displayed after the console has been closed");

// USING PROMPT
// prompt() - small window on top, asks for string input
let samplePrompt = prompt("Enter your name:");

console.log("Hello, "+samplePrompt);

let nullPrompt = prompt("do not enter anything here.");

console.log(nullPrompt);

/*MINI ACTIVITY*/
/*
	create a funtion welcomeMessage
		create 2 variables
			firstName prompt
			lastName prompt
		log "hello fn ln"
	invoke
*/

function welcomeMessage(){
	let firstName = prompt("What is your FIRST NAME?");
	let lastName = prompt("What is your LAST NAME?");
	console.log("Hello, "+firstName+" "+lastName+"!");
}

welcomeMessage();

// FUNCTION NAMING

function getCourses(){
	let courses = ["Science 101", "Math 101", "English 101"];
	console.log(courses);	
}
getCourses();

function get(){
	let name = "Jamie";
	console.log(name);
}
get();

function foo() {
	console.log(25%5);
}
foo();
