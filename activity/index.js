/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	function personalInformation() {
		let userName = prompt("What is your name?");
		let userAge = prompt("How old are you?");
		let userLoc = prompt("Where do you live?");
		console.log("Hello, "+userName);
		console.log("You are "+userAge+" years old");
		console.log("You live in "+userLoc);
		alert("Thank you for your input!");
	}

	personalInformation();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function favoriteMusicians(){
		console.log("My Favorite Song Artists/Bands");
		console.log("1. Taylor Swift");
		console.log("2. Lady Gaga");
		console.log("3. Ariana Grande");
		console.log("4. Pentatonix");
		console.log("5. Ben Platt");
		}

	favoriteMusicians();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function myMovieRatings(){
		console.log("Ratings of My Favorite Movies");
		console.log("1. Shrek 1");
		console.log("Rotten Tomatoes Rating: 88%");
		console.log("2. Shrek 2");
		console.log("Rotten Tomatoes Rating: 89%");
		console.log("3. Shrek 3");
		console.log("Rotten Tomatoes Rating: 41%");
		console.log("4. Shrek 4");
		console.log("Rotten Tomatoes Rating: 58%");
		console.log("5. Frozen 2");
		console.log("Rotten Tomatoes Rating: 77%");
	}

	myMovieRatings();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

/*printUsers();*/
/*let printFriends() = */function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
printUsers();

/*console.log(friend2);
console.log(friend1); */